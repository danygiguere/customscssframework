///<reference path="fullwidthslider.ts"/>
///<reference path="contentslider.ts"/>
///<reference path="slider.ts"/>

/*
 https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded
 The DOMContentLoaded event is fired when the initial HTML document has been completely loaded and parsed,
 without waiting for stylesheets, images, and subframes to finish loading. A very different event load
 should be used only to detect a fully-loaded page. It is an incredibly popular mistake to use load
 where DOMContentLoaded would be much more appropriate, so be cautious.
 */

window.addEventListener("load", function () {
    // INIT NUVOSLIDER
    if (document.querySelector('#nuvoslider')) {
        var new_nuvoslider = new NuvoSlider('#nuvoslider', 5000);
        new_nuvoslider.initialize();
    }
    // INIT CONTENTSLIDER
    if (document.querySelector('#contentslider')) {
        var new_contentslider = new ContentSlider('#contentslider', 5000);
        new_contentslider.initialize();
    }
    // INIT SLIDER
    if (document.querySelector('#slider')) {
        var new_slider = new Slider('#slider', 5000);
        new_slider.initialize();
    }
});

document.addEventListener('DOMContentLoaded', function () {
    var isMobile = false;
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;
    if (!isMobile) {
        if (document.querySelector('.mid-section')) {
            var midSection = document.querySelectorAll('.mid-section');
            for (var i = 0; i < midSection.length; i++) {
                midSection[i].classList.add('desktop');
            }
        }
        if (document.querySelector('.three-d-button')) {
            var threeDButtons = document.querySelectorAll('.three-d-button');
            [].forEach.call(threeDButtons, function (threeDButton) {
                threeDButton.classList.add('desktop');
            })
        }
        if (document.querySelector('.fa-pencil') || document.querySelector('.fa-trash-o')) {
            var faPencils = document.querySelectorAll('.fa-pencil');
            var faTrashOs = document.querySelectorAll('.fa-trash-o');
            [].forEach.call(faPencils, function (faPencil) {
                faPencil.classList.add('invisible');
            })
        }
    }

    /* GENERAL FUNCTIONS */
    function isElementInViewport(el) {
        var rect = el.getBoundingClientRect(),
            windowHeight = (window.innerHeight || document.documentElement.clientHeight),
            windowWidth = (window.innerWidth || document.documentElement.clientWidth);
        return (
            ((rect.top >= -rect.height) && (rect.bottom <= (windowHeight + rect.height))) &&
            ((rect.left >= -rect.width) && (rect.right <= (windowWidth + rect.width)))
        );
    }

    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        }
    }

    // MENU
    if (document.querySelector('nav')) {
        var menuButton = document.querySelector('.menu-button');
        var nav = document.querySelector('nav');
        var navRight = nav.querySelector('.nav-right');

        function toggleMenuButton() {
            if (!menuButton.classList.contains('active')) {
                menuButton.classList.add('active');
            } else {
                menuButton.classList.remove('active');
            }
        }

        function toggleMenu() {
            if (!nav.classList.contains('open')) {
                nav.classList.add('open');
                setTimeout(function () {
                    navRight.classList.remove('visuallyhidden');
                }, 20);
            } else {
                navRight.classList.add('visuallyhidden');
                setTimeout(function () {
                    nav.classList.remove('open');
                }, 300);
            }
        }

        menuButton.addEventListener("click", function () {
            toggleMenuButton();
            toggleMenu();
        }, false);

        function closeMenu(event) {
            if (!nav.contains(event.target) && !menuButton.contains(event.target)) {
                menuButton.classList.remove('active');
                navRight.classList.add('visuallyhidden');
                setTimeout(function () {
                    nav.classList.remove('open');
                }, 300);
            }
        }

        document.addEventListener('click', closeMenu);
        document.addEventListener('touchstart', closeMenu);
    }

    // MODAL
    if (document.querySelector('[data-toggle="modal"]')) {
        function closeModal(modal, initialPageYOffset) {
            window.scroll(window.pageXOffset, initialPageYOffset);
            modal.classList.add('visuallyhidden');
            setTimeout(function () {
                modal.classList.remove('open');
                document.body.classList.remove('modal-open');
            }, 1400);
        }

        function hideModal(event, modal, initialPageYOffset) {
            var modalContent = modal.querySelector('.modal-content');
            if (!modalContent.contains(event.target)) {
                closeModal(modal, initialPageYOffset);
            }
        }

        function showModal(dataTargetAttribute, initialPageYOffset) {
            var modal = document.querySelector(dataTargetAttribute + '.modal');
            modal.classList.add('open');
            document.body.classList.add('modal-open');
            setTimeout(function () {
                modal.classList.remove('visuallyhidden');
            }, 20);
            var closeButton = modal.querySelector('.close-button');
            closeButton.addEventListener("click", function () {
                closeModal(modal, initialPageYOffset);
            }, false);
            modal.addEventListener("click", function (event) {
                hideModal(event, modal, initialPageYOffset);
            }, false);
            modal.addEventListener("touchstart", function (event) {
                hideModal(event, modal, initialPageYOffset);
            }, false);
        }

        var modalButtons = document.querySelectorAll('[data-toggle="modal"]');
        [].forEach.call(modalButtons, function (modalButton) {
            var dataTargetAttribute = modalButton.getAttribute('data-target');
            modalButton.addEventListener("click", function () {
                var initialPageYOffset = window.pageYOffset;
                showModal(dataTargetAttribute, initialPageYOffset);
            }, false);
        });
    }

    // FIFTYFIFTY
    if (document.querySelector('.fiftyfifty')) {
        function check_container_width() {
            var halfContainerFirst = document.querySelectorAll('.half-container.first');
            var halfContainerSecond = document.querySelectorAll('.half-container.second');
            if (window.outerWidth >= 768) {
                var halfContainerWidth = (document.querySelector('.container').clientWidth) / 2;
                [].forEach.call(halfContainerFirst, function (element) {
                    element.style.width = halfContainerWidth + "px";
                });
                [].forEach.call(halfContainerSecond, function (element) {
                    element.style.width = halfContainerWidth + "px";
                });
            } else {
                [].forEach.call(halfContainerFirst, function (element) {
                    element.style.width = "100%";
                });
                [].forEach.call(halfContainerSecond, function (element) {
                    element.style.width = "100%";
                });
            }
        }

        window.addEventListener("resize", check_container_width);
        check_container_width();
    }

    // FADE IN EFFECTS
    if (document.querySelector('.fadeInOut') || document.querySelector('.fadeIn')) {
        function fadeIn(element, fadeOut = true) {
            var fadeInElements = function () {
                var fadeIn = document.querySelectorAll(element);
                [].forEach.call(fadeIn, function (item) {
                    if (isElementInViewport(item)) {
                        item.classList.add('in-view');
                    } else {
                        if (fadeOut == true) {
                            item.classList.remove('in-view');
                        }
                    }
                });
                // if all the images are loaded, stop calling the function
                if (fadeIn.length == 0) {
                    window.removeEventListener("DOMContentLoaded", fadeInElements);
                    window.removeEventListener("load", fadeInElements);
                    window.removeEventListener("resize", fadeInElements);
                    window.removeEventListener("scroll", fadeInElements);
                }
            };

            window.addEventListener("DOMContentLoaded", fadeInElements);
            window.addEventListener("load", fadeInElements);
            window.addEventListener("resize", fadeInElements);
            window.addEventListener("scroll", fadeInElements);
        }

        fadeIn(".fadeInOut", true);
        fadeIn(".fadeIn", false);
    }

    // LAZYLOAD
    if (document.querySelector("img[data-src]")) {
        var lazyLoadImages = function () {
            var images = document.querySelectorAll("img[data-src]");
            [].forEach.call(images, function (item) {
                if (isElementInViewport(item)) {
                    item.setAttribute("src", item.getAttribute("data-src"));
                    item.removeAttribute("data-src")
                }
            });
            // if all the images are loaded, stop calling the handler
            if (images.length == 0) {
                window.removeEventListener("DOMContentLoaded", lazyLoadImages);
                window.removeEventListener("load", lazyLoadImages);
                window.removeEventListener("resize", lazyLoadImages);
                window.removeEventListener("scroll", lazyLoadImages);
            }
        };

        window.addEventListener("DOMContentLoaded", lazyLoadImages);
        window.addEventListener("load", lazyLoadImages);
        window.addEventListener("resize", lazyLoadImages);
        window.addEventListener("scroll", lazyLoadImages);
    }

    // PROMO-BAR
    if (document.querySelector('#promo-bar')) {
        var promoBar = document.getElementById('promo-bar');
        var promoBarContainer = promoBar.querySelector('.promo-bar-container');
        var footer = document.querySelector('footer');

        var makePromoBarFixed = debounce(function () {
            promoBar.style.height = promoBarContainer.clientHeight + "px";
            var footerOffsetTop = footer.offsetTop;
            var windowOffsetTop = window.innerHeight + window.pageYOffset;
            if (footerOffsetTop >= windowOffsetTop) {
                if (!promoBarContainer.classList.contains('fixed')) {
                    promoBarContainer.classList.add('fixed');
                }
            } else {
                if (promoBarContainer.classList.contains('fixed')) {
                    promoBarContainer.classList.remove('fixed');
                }
            }
        }, 15);

        window.addEventListener("resize", makePromoBarFixed);
        window.addEventListener("scroll", makePromoBarFixed);
        makePromoBarFixed();
    }

    // SIDEBAR
    if (document.querySelector('.sidebar')) {
        var toggleSidebarFixed = debounce(function () {
            var aside = document.querySelector('.aside');
            var sidebarContainer = aside.querySelector('.sidebar-container');
            var sidebar = aside.querySelector('.sidebar');
            var mainContent = document.querySelector('.main-content');
            sidebar.style.width = sidebarContainer.clientWidth + "px";
            if (mainContent.offsetHeight > sidebarContainer.offsetHeight) {
                if (window.pageYOffset <= sidebarContainer.offsetTop) {
                    sidebar.classList.remove('fixed');
                    sidebar.classList.remove('sidebar-bottom');
                }
                else if (window.pageYOffset >= (sidebarContainer.offsetTop + aside.offsetHeight - sidebar.offsetHeight)) {
                    sidebar.classList.remove('fixed');
                    sidebar.classList.add('sidebar-bottom');
                }
                else {
                    sidebar.classList.add('fixed');
                    sidebar.classList.remove('sidebar-bottom');
                }
            }
        }, 15);

        window.addEventListener("resize", toggleSidebarFixed);
        window.addEventListener("scroll", toggleSidebarFixed);
        toggleSidebarFixed();
    }

    // SHOWMORE
    if (document.querySelector('[data-toggle="showmore"]')) {
        function toggleShowMore(showMore, showMoreButton) {
            var showMoreOffsetTop = window.pageYOffset - showMore.clientHeight;
            if (showMore.classList.contains('open')) {
                showMore.classList.add('visuallyhidden');
                setTimeout(function () {
                    showMore.classList.remove('open');
                    window.scroll(window.pageXOffset, showMoreOffsetTop);
                }, 20);
                showMoreButton.innerHTML = "Show More";
            } else {
                showMore.classList.add('open');
                setTimeout(function () {
                    showMore.classList.remove('visuallyhidden');
                }, 20);
                showMoreButton.innerHTML = "Show Less";
            }
        }

        var showMoreButtons = document.querySelectorAll('[data-toggle="showmore"]');
        [].forEach.call(showMoreButtons, function (showMoreButton) {
            var dataTargetAttribute = showMoreButton.getAttribute('data-target');
            var showMore = document.querySelector(dataTargetAttribute);
            showMoreButton.addEventListener("click", function () {
                toggleShowMore(showMore, showMoreButton);
            }, false);
        });
    }

    // TAB-BLOCK
    if (document.querySelector('.tab-block')) {
        function deactivateAllTabs(tabBlock) {
            var tabs = tabBlock.querySelectorAll('.tab');
            [].forEach.call(tabs, function (tab) {
                tab.classList.remove('active');
            });
            var tabContents = tabBlock.querySelectorAll('.tab-content');
            [].forEach.call(tabContents, function (tabContent) {
                tabContent.classList.remove('active');
            });
        }

        function activateTab(tabBlock, dataTargetAttribute) {
            deactivateAllTabs(tabBlock);
            var tab = tabBlock.querySelector(dataTargetAttribute);
            var tabContent = tabBlock.querySelector('[data-target="' + dataTargetAttribute + '"]');
            tab.classList.add('active');
            tabContent.classList.add('active');
        }

        var tabBlocks = document.querySelectorAll('.tab-block');
        [].forEach.call(tabBlocks, function (tabBlock) {
            var dataToggleTab = tabBlock.querySelectorAll('[data-toggle="tab"]');
            [].forEach.call(dataToggleTab, function (tab) {
                var dataTargetAttribute = tab.getAttribute('data-target');
                tab.addEventListener("click", function () {
                    activateTab(tabBlock, dataTargetAttribute);
                }, false);
            });
        });
    }

    // SCROLL-TO
    if(document.querySelector('.scrollTo')) {
        var targetOffset, currentPosition,
            body = document.body,
            scrollButtons = document.querySelectorAll('.scrollTo'),
            animateTime = 900;

        function getPageScroll() {
            var yScroll;

            if (window.pageYOffset) {
                yScroll = window.pageYOffset;
            } else if (document.documentElement && document.documentElement.scrollTop) {
                yScroll = document.documentElement.scrollTop;
            } else if (document.body) {
                yScroll = document.body.scrollTop;
            }
            return yScroll;
        }

        function scrollTo(event) {
            var targetWithoutHash = event.target.hash.substr(1);
            targetOffset = document.getElementById(targetWithoutHash).offsetTop;
            currentPosition = getPageScroll();


            var fixedMenuHeight = 64;
            body.classList.add('in-transition');
            body.style.WebkitTransform = "translate(0, -" + (targetOffset - currentPosition - fixedMenuHeight) + "px)";
            body.style.MozTransform = "translate(0, -" + (targetOffset - currentPosition - fixedMenuHeight) + "px)";
            body.style.transform = "translate(0, -" + (targetOffset - currentPosition - fixedMenuHeight) + "px)";

            window.setTimeout(function () {
                body.classList.remove('in-transition');
                body.style.cssText = "";
                window.scrollTo(0, targetOffset - fixedMenuHeight);
            }, animateTime);

            event.preventDefault();
        }

        [].forEach.call(scrollButtons, function (scrollButton) {
            scrollButton.addEventListener('click', function (event) {
                scrollTo(event);
            }, false);
        });
    }

    // IMAGE UPLOAD
    if (document.querySelector(".img-upload")) {
        function loadImage(event, output) {
            output.src = URL.createObjectURL(event.target.files[0]);
        }

        var imgUploads = document.querySelectorAll(".img-upload");
        [].forEach.call(imgUploads, function (imgUpload) {
            var output = imgUpload.querySelector(".post-img");
            imgUpload.addEventListener('change', function (event) {
                loadImage(event, output);
            }, false);
        });
    }

});




