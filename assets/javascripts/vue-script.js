Vue.component('tabs', {
    template: `
            <div>
                <div class="scroll">
                    <div v-for="tab in tabs" class="tab" :class="{ 'active': tab.active }" @click="selectTab(tab)">{{ tab.name }}</div>
                </div>
                <div class="tab-contents">
                    <slot></slot>
                </div>
            </div>
    `,
    data() {
        return { tabs: [] };
    },
    created() {
        this.tabs = this.$children;
    },
    methods: {
        selectTab(selectedTab) {
            this.tabs.forEach(tab => {
                tab.active = (tab.name == selectedTab.name);
            });
        }
    }
});

Vue.component('tab-content', {
    template: `
    <div class="padding tab-content" :class="{ active: active }"><slot></slot></div>
    `,
    props: {
        name: { required: true },
        selected: { default: false }
    },
    data() {
        return {
            active: false
        };
    },
    mounted() {
        this.active = this.selected;
    }
});

new Vue({
    el: '.tab-component',
});






Vue.component('modal', {
    template: `
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-container">
                    <div class="modal-header">
                        <slot name="header">
                            <h2>Header</h2>
                        </slot>
                    </div>           
                    <div class="modal-body">
                        <slot name="body">
                            <p>Body</p>
                        </slot>
                    </div>
                    <div class="modal-footer">
                        <slot name="footer">     
                            <p>footer</p>
                            <button class="modal-default-button" @click="$emit('close')">
                            OK
                            </button>
                        </slot>
                    </div>
                </div>
            </div>
        </div>
    </transition>
    `
});

// start app
new Vue({
    el: '#vue-modal',
    data: {
        showModal: false
    }
});

