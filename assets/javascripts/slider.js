var Slider = (function () {
    function Slider(selector, time) {
        var _this = this;
        this._slideNumber = 0;
        this._time = 5000;
        this.initialize = function () {
            _this.slider = document.querySelector(_this.selector);
            _this.slide = _this.slider.querySelectorAll('.slide');
            _this.slide[0].classList.add('first-active');
            _this.bulletPoint = _this.slider.querySelectorAll('.bullet-point');
            _this.bulletPoint[0].classList.add('active');
            _this.start();
            _this.enableTouch();
            window.addEventListener("resize", _this.resize, false);
            var that = _this;
            that.resize();
            [].forEach.call(_this.bulletPoint, function (element, index) {
                element.addEventListener("click", function () {
                    that.sliderGoToSlide(index);
                }, false);
            });
        };
        this.start = function () {
            _this.starter = setInterval(_this.sliderForward, _this.time);
        };
        this.restart = function () {
            clearInterval(_this.starter);
            _this.start();
        };
        this.sliderGoToSlide = function (index) {
            [].forEach.call(_this.bulletPoint, function (element) {
                element.className = 'bullet-point';
            });
            _this.bulletPoint[index].classList.add('active');
            _this.slideNumber = index;
            [].forEach.call(_this.slide, function (element) {
                element.className = "slide";
            });
            _this.slide[_this.slideNumber].classList.add('active');
            _this.restart();
        };
        this.sliderForward = function () {
            [].forEach.call(_this.bulletPoint, function (element) {
                element.className = 'bullet-point';
            });
            _this.slide[_this.slideNumber].className = "slide";
            if (_this.slideNumber == _this.slide.length - 1) {
                _this.slideNumber = 0;
            }
            else {
                _this.slideNumber++;
            }
            _this.bulletPoint[_this.slideNumber].classList.add('active');
            _this.slide[_this.slideNumber].classList.add('active');
            _this.restart();
        };
        this.sliderBackward = function () {
            [].forEach.call(_this.bulletPoint, function (element) {
                element.className = 'bullet-point';
            });
            _this.slide[_this.slideNumber].className = "slide";
            if (_this.slideNumber == 0) {
                _this.slideNumber = _this.slide.length - 1;
            }
            else {
                _this.slideNumber--;
            }
            _this.bulletPoint[_this.slideNumber].classList.add('active');
            _this.slide[_this.slideNumber].classList.add('active');
            _this.restart();
        };
        this.resize = function () {
            var that = _this;
            var highestSliderHeight = 0;
            [].forEach.call(_this.slider.querySelectorAll('.slide-container'), function (element) {
                that.sliderHeight = element.clientHeight;
                if (that.sliderHeight > highestSliderHeight) {
                    highestSliderHeight = that.sliderHeight;
                }
            });
            var highestSliderInnerHeight = 0;
            [].forEach.call(_this.slider.querySelectorAll('.slide-container'), function (element) {
                that.sliderInnerHeight = element.clientHeight;
                if (that.sliderInnerHeight > highestSliderInnerHeight) {
                    highestSliderInnerHeight = that.sliderInnerHeight;
                }
            });
            _this.bulletPoints = _this.slider.querySelector('.bullet-points');
            _this.bulletPointsHeight = _this.bulletPoints.clientHeight;
            _this.slider.style.height = highestSliderHeight + _this.bulletPointsHeight + "px";
            _this.bulletPoints.style.marginTop = highestSliderInnerHeight + "px";
        };
        this.enableTouch = function () {
            (function (d) {
                var ce = function (e, n) {
                    var a = document.createEvent("CustomEvent");
                    a.initCustomEvent(n, true, true, e.target);
                    e.target.dispatchEvent(a);
                    a = null;
                    return false;
                }, nm = true, sp = { x: 0, y: 0 }, ep = { x: 0, y: 0 }, touch = {
                    touchstart: function (e) {
                        sp = { x: e.touches[0].pageX, y: e.touches[0].pageY };
                    },
                    touchmove: function (e) {
                        nm = false;
                        ep = { x: e.touches[0].pageX, y: e.touches[0].pageY };
                    },
                    touchend: function (e) {
                        if (nm) {
                            ce(e, 'fc');
                        }
                        else {
                            var x = ep.x - sp.x, xr = Math.abs(x), y = ep.y - sp.y, yr = Math.abs(y);
                            if (Math.max(xr, yr) > 20) {
                                ce(e, (xr > yr ? (x < 0 ? 'swl' : 'swr') : (y < 0 ? 'swu' : 'swd')));
                            }
                        }
                        ;
                        nm = true;
                    },
                    touchcancel: function (e) {
                        nm = false;
                    }
                };
                for (var a in touch) {
                    d.addEventListener(a, touch[a], false);
                }
            })(document);
            var h = function (e) {
                console.log(e.type, e);
            };
            _this.slider.addEventListener('swl', _this.sliderForward, false);
            _this.slider.addEventListener('swr', _this.sliderBackward, false);
        };
        this.selector = selector;
        this.time = time;
    }
    Object.defineProperty(Slider.prototype, "selector", {
        get: function () {
            return this._selector;
        },
        set: function (selector) {
            this._selector = selector;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "slider", {
        get: function () {
            return this._slider;
        },
        set: function (slider) {
            this._slider = slider;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "slide", {
        get: function () {
            return this._slide;
        },
        set: function (slide) {
            this._slide = slide;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "bulletPoint", {
        get: function () {
            return this._bulletPoint;
        },
        set: function (bulletPoint) {
            this._bulletPoint = bulletPoint;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "bulletPoints", {
        get: function () {
            return this._bulletPoints;
        },
        set: function (bulletPoints) {
            this._bulletPoints = bulletPoints;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "bulletPointsHeight", {
        get: function () {
            return this._bulletPointsHeight;
        },
        set: function (bulletPointsHeight) {
            this._bulletPointsHeight = bulletPointsHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "sliderHeight", {
        get: function () {
            return this._sliderHeight;
        },
        set: function (sliderHeight) {
            this._sliderHeight = sliderHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "sliderInnerHeight", {
        get: function () {
            return this._sliderInnerHeight;
        },
        set: function (sliderInnerHeight) {
            this._sliderInnerHeight = sliderInnerHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "starter", {
        get: function () {
            return this._starter;
        },
        set: function (starter) {
            this._starter = starter;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "slideNumber", {
        get: function () {
            return this._slideNumber;
        },
        set: function (slideNumber) {
            this._slideNumber = slideNumber;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Slider.prototype, "time", {
        get: function () {
            return this._time;
        },
        set: function (time) {
            if (time > 0) {
                this._time = time;
            }
            else {
                console.log("error, time entered cannot be negative");
                this._time = 5000;
            }
        },
        enumerable: true,
        configurable: true
    });
    return Slider;
}());
