// SLIDER
if (document.querySelector('#nuvoslider')) {
    var starter;
    var slideNumber = 0;
    var time = 5000;
    var touchClick = 0;
    var slide = document.querySelectorAll('.slide');
    slide[0].classList.add('first-active');
    var arrows = document.querySelectorAll('.slider-control');
    enableTouch();
    start();
    document.querySelector(".slider-previous").addEventListener("click", sliderBackward, false);
    document.querySelector(".slider-next").addEventListener("click", sliderForward, false);

    function start() {
        starter = setInterval(sliderForward, time);
    }

    function restart() {
        clearInterval(starter);
        start();
    }

    function sliderForward() {
        slide[slideNumber].className = "slide";
        if (slideNumber == slide.length - 1) {
            slideNumber = 0;
        } else {
            slideNumber++;
        }
        slide[slideNumber].classList.add('active');
        restart();
    }

    function sliderBackward() {
        slide[slideNumber].className = "slide";
        slideNumber--;
        if (slideNumber == -1) {
            slideNumber = slide.length - 1;
        }
        slide[slideNumber].classList.add('active');
        restart();
    }

    function enableTouch() {
        (function (d) {
            var ce = function (e, n) {
                var a = document.createEvent("CustomEvent");
                a.initCustomEvent(n, true, true, e.target);
                e.target.dispatchEvent(a);
                a = null;
                return false;
            }, nm = true, sp = {x: 0, y: 0}, ep = {x: 0, y: 0}, touch = {
                touchstart: function (e) {
                    sp = {x: e.touches[0].pageX, y: e.touches[0].pageY};
                },
                touchmove: function (e) {
                    nm = false;
                    ep = {x: e.touches[0].pageX, y: e.touches[0].pageY};
                },
                touchend: function (e) {
                    if (nm) {
                        ce(e, 'fc');
                    }
                    else {
                        var x = ep.x - sp.x, xr = Math.abs(x), y = ep.y - sp.y, yr = Math.abs(y);
                        if (Math.max(xr, yr) > 20) {
                            ce(e, (xr > yr ? (x < 0 ? 'swl' : 'swr') : (y < 0 ? 'swu' : 'swd')));
                        }
                    }
                    ;
                    nm = true;
                },
                touchcancel: function (e) {
                    nm = false;
                }
            };
            for (var a in touch) {
                d.addEventListener(a, touch[a], false);
            }
        })(document);
        var h = function (e) {
            console.log(e.type, e);
        };
        document.body.addEventListener('fc', toggleArrows, false);
        document.body.addEventListener('swl', sliderForward, false);
        document.body.addEventListener('swr', sliderBackward, false);
    }

    function isHover(e) {
        return (e.parentElement.querySelector(':hover') === e);
    }

    function toggleArrows() {
        if (isHover(document.querySelector(".slider-previous")) || isHover(document.querySelector(".slider-next"))) {
            arrows[0].style.opacity = "1";
            arrows[1].style.opacity = "1";
        }
        else {
            if (touchClick == 2) {
                touchClick = 0;
            }
            if (touchClick % 2 == 0) {
                arrows[0].style.opacity = "1";
                arrows[1].style.opacity = "1";
            }
            else {
                arrows[0].style.opacity = "0";
                arrows[1].style.opacity = "0";
            }
            touchClick++;
        }
    }
}