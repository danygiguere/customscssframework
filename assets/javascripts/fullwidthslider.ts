class NuvoSlider {
    private _selector: string;
    private _slider: Element;
    private _slide: Element;
    private _starter: number;
    private _arrows: any;
    private _slideNumber: number = 0;
    private _time: number = 5000;
    private _touchClick: number = 0;

    constructor(selector, time) {
        this.selector = selector;
        this.time = time;
    };

    public initialize = () => {
        this.slider = document.querySelector(this.selector);
        this.slide = this.slider.querySelectorAll('.slide');
        this.slide[0].classList.add('first-active');
        this.arrows = this.slider.querySelectorAll('.slider-control');
        this.enableTouch();
        this.start();
        this.slider.querySelector(".slider-previous").addEventListener("click", this.sliderBackward, false);
        this.slider.querySelector(".slider-next").addEventListener("click", this.sliderForward, false);
    };

    public start = () => {
        this.starter = setInterval(this.sliderForward, this.time);
    };

    public restart = () => {
        clearInterval(this.starter);
        this.start();
    };

    public sliderForward = () => {
        this.slide[this.slideNumber].className = "slide";
        if (this.slideNumber == this.slide.length - 1) {
            this.slideNumber = 0;
        } else {
            this.slideNumber++;
        }
        this.slide[this.slideNumber].classList.add('active');
        this.restart();
    };

    public sliderBackward = () => {
        this.slide[this.slideNumber].className = "slide";
        this.slideNumber--;
        if (this.slideNumber == -1) {
            this.slideNumber = this.slide.length - 1;
        }
        this.slide[this.slideNumber].classList.add('active');
        this.restart();
    };

    public enableTouch = () => {
        (function (d) {
            var ce = function (e, n) {
                    var a = document.createEvent("CustomEvent");
                    a.initCustomEvent(n, true, true, e.target);
                    e.target.dispatchEvent(a);
                    a = null;
                    return false
                },
                nm = true, sp = {x: 0, y: 0}, ep = {x: 0, y: 0},
                touch = {
                    touchstart: function (e) {
                        sp = {x: e.touches[0].pageX, y: e.touches[0].pageY}
                    },
                    touchmove: function (e) {
                        nm = false;
                        ep = {x: e.touches[0].pageX, y: e.touches[0].pageY}
                    },
                    touchend: function (e) {
                        if (nm) {
                            ce(e, 'fc')
                        } else {
                            var x = ep.x - sp.x, xr = Math.abs(x), y = ep.y - sp.y, yr = Math.abs(y);
                            if (Math.max(xr, yr) > 20) {
                                ce(e, (xr > yr ? (x < 0 ? 'swl' : 'swr') : (y < 0 ? 'swu' : 'swd')))
                            }
                        }
                        ;
                        nm = true
                    },
                    touchcancel: function (e) {
                        nm = false
                    }
                };
            for (var a in touch) {
                d.addEventListener(a, touch[a], false);
            }
        })(document);
        var h = function (e) {
            console.log(e.type, e)
        };
        this.slider.addEventListener('fc', this.toggleArrows, false); // 0-50ms = fast-clicks vs 500ms for normal clicks
        this.slider.addEventListener('swl', this.sliderForward, false);
        this.slider.addEventListener('swr', this.sliderBackward, false);
    };

    public isHover(e) {
        return (e.parentElement.querySelector(':hover') === e);
    };

    public toggleArrows = () => {
        if (this.isHover(this.slider.querySelector(".slider-previous")) || this.isHover(this.slider.querySelector(".slider-next"))) {
            this.arrows[0].style.opacity = "1";
            this.arrows[1].style.opacity = "1";
        } else {
            if (this.touchClick == 2) {
                this.touchClick = 0
            }
            if (this.touchClick % 2 == 0) {
                this.arrows[0].style.opacity = "1";
                this.arrows[1].style.opacity = "1";
            } else {
                this.arrows[0].style.opacity = "0";
                this.arrows[1].style.opacity = "0";
            }
            this.touchClick++;
        }
    };

    get selector(): string {
        return this._selector;
    }

    set selector(selector: string) {
        this._selector = selector;
    }

    get slider(): Element {
        return this._slider;
    }

    set slider(slider: Element) {
        this._slider = slider;
    }

    get slide(): any {
        return this._slide;
    }

    set slide(slide: any) {
        this._slide = slide;
    }

    get arrows(): any {
        return this._arrows;
    }

    set arrows(arrows: any) {
        this._arrows = arrows;
    }

    get starter(): number {
        return this._starter;
    }

    set starter(starter: number) {
        this._starter = starter;
    }

    get slideNumber(): number {
        return this._slideNumber;
    }

    set slideNumber(slideNumber: number) {
        this._slideNumber = slideNumber;
    }

    get touchClick(): number {
        return this._touchClick;
    }

    set touchClick(touchClick: number) {
        this._touchClick = touchClick;
    }

    get time(): number {
        return this._time;
    }

    set time(time: number) {
        if (time > 0) {
            this._time = time;
        } else {
            console.log("error, time entered cannot be negative");
            this._time = 5000;
        }
    }

}