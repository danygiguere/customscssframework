class ContentSlider {
    private _selector: string;
    private _slider: any;
    private _slide: Element;
    private _starter: number;
    private _bulletPoint: any;
    private _bulletPoints: any;
    private _bulletPointsHeight: number;
    private _contentsliderHeight: number;
    private _contentsliderInnerHeight: number;
    private _slideNumber: number = 0;
    private _time: number = 5000;

    constructor(selector, time) {
        this.selector = selector;
        this.time = time;
    }

    public initialize = () => {
        this.slider = document.querySelector(this.selector);
        this.slide = this.slider.querySelectorAll('.slide');
        this.slide[0].classList.add('first-active');
        this.bulletPoint = this.slider.querySelectorAll('.bullet-point');
        this.bulletPoint[0].classList.add('active');
        this.start();
        window.addEventListener("resize", this.resize, false);
        var that = this;
        that.resize();
        [].forEach.call(this.bulletPoint, function (element, index) {
            element.addEventListener("click", function () {
                that.sliderGoToSlide(index);
            }, false);
        });
    };

    public start = () => {
        this.starter = setInterval(this.sliderForward, this.time);
    };

    public restart = () => {
        clearInterval(this.starter);
        this.start();
    };

    public sliderGoToSlide = (index) => {
        [].forEach.call(this.bulletPoint, function (element) {
            element.className = 'bullet-point';
        });
        this.bulletPoint[index].classList.add('active');
        this.slideNumber = index;
        [].forEach.call(this.slide, function (element) {
            element.className = "slide";
        });
        this.slide[this.slideNumber].classList.add('active');
        this.restart();
    };

    public sliderForward = () => {
        [].forEach.call(this.bulletPoint, function (element) {
            element.className = 'bullet-point';
        });
        this.slide[this.slideNumber].className = "slide";
        if (this.slideNumber == this.slide.length - 1) {
            this.slideNumber = 0;
        } else {
            this.slideNumber++;
        }
        this.bulletPoint[this.slideNumber].classList.add('active');
        this.slide[this.slideNumber].classList.add('active');
        this.restart();
    };

    public resize = () => {
        var that = this;
        var highestContentsliderHeight = 0;
        [].forEach.call(this.slider.querySelectorAll('.slide'), function (element) {
            that.contentsliderHeight = element.clientHeight;
            if(that.contentsliderHeight > highestContentsliderHeight) {
                highestContentsliderHeight = that.contentsliderHeight;
            }
        });
        this.bulletPoints = this.slider.querySelector('.bullet-points');
        this.bulletPointsHeight = this.bulletPoints.clientHeight;
        this.slider.style.height = highestContentsliderHeight + this.bulletPointsHeight + "px";
        this.bulletPoints.style.marginTop = highestContentsliderHeight + "px";
    };

    get selector(): string {
        return this._selector;
    }

    set selector(selector: string) {
        this._selector = selector;
    }

    get slider(): any {
        return this._slider;
    }

    set slider(slider: any) {
        this._slider = slider;
    }

    get slide(): any {
        return this._slide;
    }

    set slide(slide: any) {
        this._slide = slide;
    }

    get bulletPoint(): any {
        return this._bulletPoint;
    }

    set bulletPoint(bulletPoint: any) {
        this._bulletPoint = bulletPoint;
    }

    get bulletPoints(): any {
        return this._bulletPoints;
    }

    set bulletPoints(bulletPoints: any) {
        this._bulletPoints = bulletPoints;
    }

    get bulletPointsHeight(): number {
        return this._bulletPointsHeight;
    }

    set bulletPointsHeight(bulletPointsHeight: number) {
        this._bulletPointsHeight = bulletPointsHeight;
    }

    get contentsliderHeight(): number {
        return this._contentsliderHeight;
    }

    set contentsliderHeight(contentsliderHeight: number) {
        this._contentsliderHeight = contentsliderHeight;
    }

    get contentsliderInnerHeight(): number {
        return this._contentsliderInnerHeight;
    }

    set contentsliderInnerHeight(contentsliderInnerHeight: number) {
        this._contentsliderInnerHeight = contentsliderInnerHeight;
    }

    get starter(): number {
        return this._starter;
    }

    set starter(starter: number) {
        this._starter = starter;
    }

    get slideNumber(): number {
        return this._slideNumber;
    }

    set slideNumber(slideNumber: number) {
        this._slideNumber = slideNumber;
    }

    get time(): number {
        return this._time;
    }

    set time(time: number) {
        if (time > 0) {
            this._time = time;
        } else {
            console.log("error, time entered cannot be negative");
            this._time = 5000;
        }
    }

}