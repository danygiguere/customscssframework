class Slider {
    private _selector: string;
    private _slider: any;
    private _slide: Element;
    private _starter: number;
    private _bulletPoint: any;
    private _bulletPoints: any;
    private _bulletPointsHeight: number;
    private _sliderHeight: number;
    private _sliderInnerHeight: number;
    private _slideNumber: number = 0;
    private _time: number = 5000;

    constructor(selector, time) {
        this.selector = selector;
        this.time = time;
    }

    public initialize = () => {
        this.slider = document.querySelector(this.selector);
        this.slide = this.slider.querySelectorAll('.slide');
        this.slide[0].classList.add('first-active');
        this.bulletPoint = this.slider.querySelectorAll('.bullet-point');
        this.bulletPoint[0].classList.add('active');
        this.start();
        this.enableTouch();
        window.addEventListener("resize", this.resize, false);
        var that = this;
        that.resize();
        [].forEach.call(this.bulletPoint, function (element, index) {
            element.addEventListener("click", function () {
                that.sliderGoToSlide(index);
            }, false);
        });
    };

    public start = () => {
        this.starter = setInterval(this.sliderForward, this.time);
    };

    public restart = () => {
        clearInterval(this.starter);
        this.start();
    };

    public sliderGoToSlide = (index) => {
        [].forEach.call(this.bulletPoint, function (element) {
            element.className = 'bullet-point';
        });
        this.bulletPoint[index].classList.add('active');
        this.slideNumber = index;
        [].forEach.call(this.slide, function (element) {
            element.className = "slide";
        });
        this.slide[this.slideNumber].classList.add('active');
        this.restart();
    };

    public sliderForward = () => {
        [].forEach.call(this.bulletPoint, function (element) {
            element.className = 'bullet-point';
        });
        this.slide[this.slideNumber].className = "slide";
        if (this.slideNumber == this.slide.length - 1) {
            this.slideNumber = 0;
        } else {
            this.slideNumber++;
        }
        this.bulletPoint[this.slideNumber].classList.add('active');
        this.slide[this.slideNumber].classList.add('active');
        this.restart();
    };

    public sliderBackward = () => {
        [].forEach.call(this.bulletPoint, function (element) {
            element.className = 'bullet-point';
        });
        this.slide[this.slideNumber].className = "slide";
        if (this.slideNumber == 0) {
            this.slideNumber = this.slide.length - 1;
        } else {
            this.slideNumber--;
        }
        this.bulletPoint[this.slideNumber].classList.add('active');
        this.slide[this.slideNumber].classList.add('active');
        this.restart();
    };

    public resize = () => {
        var that = this;
        var highestSliderHeight = 0;
        [].forEach.call(this.slider.querySelectorAll('.slide-container'), function (element) {
            that.sliderHeight = element.clientHeight;
            if(that.sliderHeight > highestSliderHeight) {
                highestSliderHeight = that.sliderHeight;
            }
        });
        var highestSliderInnerHeight = 0;
        [].forEach.call(this.slider.querySelectorAll('.slide-container'), function (element) {
            that.sliderInnerHeight = element.clientHeight;
            if(that.sliderInnerHeight > highestSliderInnerHeight) {
                highestSliderInnerHeight = that.sliderInnerHeight;
            }
        });
        this.bulletPoints = this.slider.querySelector('.bullet-points');
        this.bulletPointsHeight = this.bulletPoints.clientHeight;
        this.slider.style.height = highestSliderHeight + this.bulletPointsHeight + "px";
        this.bulletPoints.style.marginTop = highestSliderInnerHeight + "px";
    };

    public enableTouch = () => {
        (function (d) {
            var ce = function (e, n) {
                    var a = document.createEvent("CustomEvent");
                    a.initCustomEvent(n, true, true, e.target);
                    e.target.dispatchEvent(a);
                    a = null;
                    return false
                },
                nm = true, sp = {x: 0, y: 0}, ep = {x: 0, y: 0},
                touch = {
                    touchstart: function (e) {
                        sp = {x: e.touches[0].pageX, y: e.touches[0].pageY}
                    },
                    touchmove: function (e) {
                        nm = false;
                        ep = {x: e.touches[0].pageX, y: e.touches[0].pageY}
                    },
                    touchend: function (e) {
                        if (nm) {
                            ce(e, 'fc')
                        } else {
                            var x = ep.x - sp.x, xr = Math.abs(x), y = ep.y - sp.y, yr = Math.abs(y);
                            if (Math.max(xr, yr) > 20) {
                                ce(e, (xr > yr ? (x < 0 ? 'swl' : 'swr') : (y < 0 ? 'swu' : 'swd')))
                            }
                        }
                        ;
                        nm = true
                    },
                    touchcancel: function (e) {
                        nm = false
                    }
                };
            for (var a in touch) {
                d.addEventListener(a, touch[a], false);
            }
        })(document);
        var h = function (e) {
            console.log(e.type, e)
        };
        this.slider.addEventListener('swl', this.sliderForward, false);
        this.slider.addEventListener('swr', this.sliderBackward, false);
    };

    get selector(): string {
        return this._selector;
    }

    set selector(selector: string) {
        this._selector = selector;
    }

    get slider(): any {
        return this._slider;
    }

    set slider(slider: any) {
        this._slider = slider;
    }

    get slide(): any {
        return this._slide;
    }

    set slide(slide: any) {
        this._slide = slide;
    }

    get bulletPoint(): any {
        return this._bulletPoint;
    }

    set bulletPoint(bulletPoint: any) {
        this._bulletPoint = bulletPoint;
    }

    get bulletPoints(): any {
        return this._bulletPoints;
    }

    set bulletPoints(bulletPoints: any) {
        this._bulletPoints = bulletPoints;
    }

    get bulletPointsHeight(): number {
        return this._bulletPointsHeight;
    }

    set bulletPointsHeight(bulletPointsHeight: number) {
        this._bulletPointsHeight = bulletPointsHeight;
    }

    get sliderHeight(): number {
        return this._sliderHeight;
    }

    set sliderHeight(sliderHeight: number) {
        this._sliderHeight = sliderHeight;
    }

    get sliderInnerHeight(): number {
        return this._sliderInnerHeight;
    }

    set sliderInnerHeight(sliderInnerHeight: number) {
        this._sliderInnerHeight = sliderInnerHeight;
    }

    get starter(): number {
        return this._starter;
    }

    set starter(starter: number) {
        this._starter = starter;
    }

    get slideNumber(): number {
        return this._slideNumber;
    }

    set slideNumber(slideNumber: number) {
        this._slideNumber = slideNumber;
    }

    get time(): number {
        return this._time;
    }

    set time(time: number) {
        if (time > 0) {
            this._time = time;
        } else {
            console.log("error, time entered cannot be negative");
            this._time = 5000;
        }
    }

}