var ContentSlider = (function () {
    function ContentSlider(selector, time) {
        var _this = this;
        this._slideNumber = 0;
        this._time = 5000;
        this.initialize = function () {
            _this.slider = document.querySelector(_this.selector);
            _this.slide = _this.slider.querySelectorAll('.slide');
            _this.slide[0].classList.add('first-active');
            _this.bulletPoint = _this.slider.querySelectorAll('.bullet-point');
            _this.bulletPoint[0].classList.add('active');
            _this.start();
            window.addEventListener("resize", _this.resize, false);
            var that = _this;
            that.resize();
            [].forEach.call(_this.bulletPoint, function (element, index) {
                element.addEventListener("click", function () {
                    that.sliderGoToSlide(index);
                }, false);
            });
        };
        this.start = function () {
            _this.starter = setInterval(_this.sliderForward, _this.time);
        };
        this.restart = function () {
            clearInterval(_this.starter);
            _this.start();
        };
        this.sliderGoToSlide = function (index) {
            [].forEach.call(_this.bulletPoint, function (element) {
                element.className = 'bullet-point';
            });
            _this.bulletPoint[index].classList.add('active');
            _this.slideNumber = index;
            [].forEach.call(_this.slide, function (element) {
                element.className = "slide";
            });
            _this.slide[_this.slideNumber].classList.add('active');
            _this.restart();
        };
        this.sliderForward = function () {
            [].forEach.call(_this.bulletPoint, function (element) {
                element.className = 'bullet-point';
            });
            _this.slide[_this.slideNumber].className = "slide";
            if (_this.slideNumber == _this.slide.length - 1) {
                _this.slideNumber = 0;
            }
            else {
                _this.slideNumber++;
            }
            _this.bulletPoint[_this.slideNumber].classList.add('active');
            _this.slide[_this.slideNumber].classList.add('active');
            _this.restart();
        };
        this.resize = function () {
            var that = _this;
            var highestContentsliderHeight = 0;
            [].forEach.call(_this.slider.querySelectorAll('.slide'), function (element) {
                that.contentsliderHeight = element.clientHeight;
                if (that.contentsliderHeight > highestContentsliderHeight) {
                    highestContentsliderHeight = that.contentsliderHeight;
                }
            });
            _this.bulletPoints = _this.slider.querySelector('.bullet-points');
            _this.bulletPointsHeight = _this.bulletPoints.clientHeight;
            _this.slider.style.height = highestContentsliderHeight + _this.bulletPointsHeight + "px";
            _this.bulletPoints.style.marginTop = highestContentsliderHeight + "px";
        };
        this.selector = selector;
        this.time = time;
    }
    Object.defineProperty(ContentSlider.prototype, "selector", {
        get: function () {
            return this._selector;
        },
        set: function (selector) {
            this._selector = selector;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "slider", {
        get: function () {
            return this._slider;
        },
        set: function (slider) {
            this._slider = slider;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "slide", {
        get: function () {
            return this._slide;
        },
        set: function (slide) {
            this._slide = slide;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "bulletPoint", {
        get: function () {
            return this._bulletPoint;
        },
        set: function (bulletPoint) {
            this._bulletPoint = bulletPoint;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "bulletPoints", {
        get: function () {
            return this._bulletPoints;
        },
        set: function (bulletPoints) {
            this._bulletPoints = bulletPoints;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "bulletPointsHeight", {
        get: function () {
            return this._bulletPointsHeight;
        },
        set: function (bulletPointsHeight) {
            this._bulletPointsHeight = bulletPointsHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "contentsliderHeight", {
        get: function () {
            return this._contentsliderHeight;
        },
        set: function (contentsliderHeight) {
            this._contentsliderHeight = contentsliderHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "contentsliderInnerHeight", {
        get: function () {
            return this._contentsliderInnerHeight;
        },
        set: function (contentsliderInnerHeight) {
            this._contentsliderInnerHeight = contentsliderInnerHeight;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "starter", {
        get: function () {
            return this._starter;
        },
        set: function (starter) {
            this._starter = starter;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "slideNumber", {
        get: function () {
            return this._slideNumber;
        },
        set: function (slideNumber) {
            this._slideNumber = slideNumber;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContentSlider.prototype, "time", {
        get: function () {
            return this._time;
        },
        set: function (time) {
            if (time > 0) {
                this._time = time;
            }
            else {
                console.log("error, time entered cannot be negative");
                this._time = 5000;
            }
        },
        enumerable: true,
        configurable: true
    });
    return ContentSlider;
}());
